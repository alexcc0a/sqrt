package com.nesterov;

public class SQRT {
    int n; // Размер массива.
    int blockSize; // Размер блока.
    int[] arr; // Исходный массива.
    int[] block; // Массив блоков.

    public SQRT(int[] a) {
        n = a.length;
        blockSize = (int) Math.sqrt(n);
        arr = new int[n];
        block = new int[blockSize + 1];

        // Копируем исходный массив.
        for (int i = 0; i < n; i++) {
            arr[i] = a[i];
        }

        // Предварительная обработка блоков.
        for (int i = 0; i < n; i++) {
            block[i / blockSize] += arr[i];
        }
    }

    // Запрос суммы на диапазоне [l, r].
    public int query(int l, int r) {
        int sum = 0;

        // Если l и r находятся в одном блоке.
        if (l / blockSize == r / blockSize) {
            for (int i = l; i <= r; i++) {
                sum += arr[i];
            }
        } else {
            // Обрабатываем левый блок.
            for (int i = l; i < (l / blockSize + 1) * blockSize; i++) {
                sum += arr[i];
            }

            // Обрабатываем целые блоки в середине диапазона.
            for (int i = l / blockSize + 1; i < r / blockSize; i++) {
                sum += block[i];
            }

            // Обрабатываем правый блок.
            for (int i = r; i >= r / blockSize * blockSize; i--) {
                sum += arr[i];
            }
        }
        return sum;
    }

    // Изменение элемента массива a[i] на x.
    public void update(int i, int x) {
        int oldVal = arr[i];
        arr[i] = x;
        block[i / blockSize] += x - oldVal;
    }

    public static void main(String[] args) {
        int[] a = {1, 3, 5, 7, 9, 11};
        SQRT sd = new SQRT(a);
        System.out.println(sd.query(1, 4));
        sd.update(2, 6);
        System.out.println(sd.query(1, 4));
    }
}
